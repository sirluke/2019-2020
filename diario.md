# Diario lezioni

Nota bene: a lezione non è detto si riesca ad affrontare tutti gli argomenti, ma per l'esame vanno conosciuti i capitoli del libro elencati nel README.

## Lezioni

### Future

Le date "future" cambiano (riprogrammazione in funzione di eventi vari) man mano che andiamo avanti col corso.
Non sono in perfetto ordine di presentazione.

### Passate

* 2020-06-05 (v) esercizi Plummino (3), OTA
* 2020-06-03 (v) esercizi Plummino (2)
* 2020-05-29 (v) esercizi Plummino (1)
* 2020-05-27 (v) esercizi FanLoop (vari), inizio Plummino
* 2020-05-22 (v) esercizio FanLoop + PID
* 2020-05-20 (v) seriale (XON/XOFF), watchdog, PWM "dei poveri" vs. PWM "vera", FanLoop adattivo (ma poco PID), PID
* 2020-05-15 (v) esercizio MQTT
* 2020-05-13 (v) recap cooperative multitask, rete, protocolli, MQTT
* 2020-05-08 (v) TaskScheduler e multitasking cooperativo
* 2020-05-06 (v) bottone e interrupt (rimbalzi), mem (EEPROM, F macro, PROGMEM, mappa mem, esptool, avrdude)
* 2020-04-29 (v) motori, servo e stepper
* 2020-04-22 (v) oscilloscopio, LA, bitbanging "visto", interrupt
* 2020-04-22 (v) pullup e pulldown, i/o, bitbanging
* 2020-04-17 (v) intervento Carraturo
* 2020-04-15 (v) conclusione elettronica, panoramica sensori e attuatori
* 2020-04-10 (v) condensatore, esperimento Arduino curva di carica e scarica, ronzatore, trasformatore, intro FanLoop
* 2020-04-08 (v) condensatori, RC, proseguimento panoramica componenti, PWM
* 2020-04-03 (v) partitore resistivo e potenziometro, lettura da Arduino
* 2020-04-01 (v) Kirchhoff, ac/dc, forme d'onda, serie (manca parallelo), voltmetro e amperometro, partitore
* 2020-03-27 (v) primi passi GPIO, breadboard, led, resistenze
* 2020-03-25 (v) legge di Ohm, resistenze
* 2020-03-20 (v) primi passi IDE
* 2020-03-18 (v) intro sistemi embedded, fino a "conversione ad/da escluso"
* 2020-03-13 (v) intro corso, prove tecniche video conf

## Argomenti da evadere (non in ordine di presentazione, non riusciremo a trattare tutti i topic in aula)

FIXME cfr. file lista domande
