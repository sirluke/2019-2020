#!/bin/bash

# parametro: nome dir (senza path)
# 1) "osserva" (in realtà cicla su) la dir studenti
# 2) compila e flasha il più recente
# 3) pubblica (via MQTT?) un feedback (e.g., dalla seriale)
# 4) attende un po' (1 minuto?) prima di passare al prossimo
# [nel frattempo la cam può essere puntata sull'oggetto]

if
 test $# -ne 1
then
 echo Usage: $0 '<sketch dir (without path)>'
 exit 1
fi

SKETCH=$1

COM=/dev/ttyACM0
echo "(suppone board agganciata a $COM)"
sudo stty -F $COM 115200

DIR=Studenti
echo "(dir studenti: $DIR)"
find $DIR -type f -exec chmod a-x {} \;

TOUT="5"
echo "(timeout: $TOUT)"

LINEE=80
echo "(linee di output: $LINEE)"

mqtt(){
 mosquitto_pub -h atrent.it -t SistEmbed/arduino-cli$2 -m "$1"
}

processaSeriale(){
	sed '/^\s*$/d' $COM | head -n $LINEE |nl| while
		read line
	do
     echo "[$dir] $line"
     mqtt "$line" "/$dir"
	done | tee $dir/$SKETCH.run
}

compilaLancia(){
 echo ___ $1 ___
 if
  arduino-cli compile --fqbn arduino:avr:uno $1
 then
  if
   arduino-cli upload -p $COM --fqbn arduino:avr:uno $1
  then
   mqtt $dir /running

   #echo SUCCESS, lanciare monitor con timeout
   #timeout -v $TOUT processaSeriale

   processaSeriale

   #cat $COM
  fi
 fi
}

while
 echo === $(date) ===
do
 for dir in $(find $DIR -type d |sort |grep $SKETCH|grep -v '\.st'|grep -v Trentini)
 do
  echo +++ $dir +++
  compilaLancia $dir
  #mqtt senzatopic
  #mqtt "contopic sadlkjasldk asldj asldj alskdj" /TOPIC
 done
done
