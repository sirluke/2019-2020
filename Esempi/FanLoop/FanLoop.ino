/* (atrent main)
 * 
 * 1) scegliere e provare lib PID
 * 
 * 2) ragionare su periodpwm
 * 
 * 3) ragionare su media mobile (e cercare/provare lib)
 * 
 */


/* versione "grande"
	LOW  800
	HIGH 250 non meno, se no non fa nemmeno in tempo a partire
	minima durata circa 20ms, max circa 60

   versione "piccola", ad esempio
    LOW  500
    HIGH 500
*/

//#define SENSORE 4  // sulla board Arduino UNO solo 2,3
#define SENSORE 2
#define ATTUATORE 3

#define RUMORE 10  // soglia minima interrupt

// TODO 'volatile' (vedere se si può creare un esperimento ad-hoc, non banale)

volatile long timestamp=0;
volatile long durata=0;
volatile long durataMedia=0; // media mobile (molto corta, a 2 elementi)

long durataDesiderata=70; // inverso velocità (10 fin troppo veloce, 180 limite di lentezza)
float duty=.5; // %
int periodpwm=1000; //millis
boolean dutyOn=false;
#define MIN .2   // sotto diventa troppo instabile

// TaskScheduler
#include <TaskScheduler.h>
Scheduler runner;

void pwmDeiPovery();
Task pwmDeiPoveryTask(periodpwm*(1-duty), TASK_FOREVER, pwmDeiPovery);

void plot();
Task plotTask(200*TASK_MILLISECOND, TASK_FOREVER, plot);

void pwmDeiPovery() {
//Task pwmDeiPoveryTask(periodpwm*(1-duty), TASK_FOREVER, [](){
    //Serial.println("pwm");
    //Serial.println(dutyOn);

    noInterrupts(); // non compila in fn anonima
    if(dutyOn) {
        digitalWrite(ATTUATORE,HIGH);
        pwmDeiPoveryTask.setInterval(periodpwm*duty);
    } else {
        digitalWrite(ATTUATORE,LOW);
        pwmDeiPoveryTask.setInterval(periodpwm*(1-duty));
    }
    interrupts(); // non compila in fn anonima

    dutyOn=!dutyOn;

	// funzione d'errore (misura la distanza dal target)
	//long errore=durataMedia-durataDesiderata; // un po' barante (introduce un appiattimento)
	long errore=durata-durataDesiderata; // reale

	// funzione di trasferimento (calcola variabili di controllo dello stato in funzione delle variabili al passo precedente e della funzione d'errore)
	// f(errore,duty) => duty

    // "strategia" LOOPBACK!  [Funzione di trasferimento]
    duty=duty+(errore*.0001);
    //duty+=(durataMedia-durataDesiderata)*.001;
    
    //Serial.println(duty);

    // capping
    if(duty>=1) duty=1.0; // in ogni caso più di 100% non ha senso, il relé resta attivo sempre
    if(duty<MIN) duty=MIN; // troppo lento, si ferma ventola

//});
}

void plot() {
    //boolean scattato=digitalRead(SENSORE); // non serve più
    //Serial.print(timestamp);
    //Serial.print(F("durata:"));
    //Serial.print(durata);
    Serial.print(F("media:"));
    Serial.print(durataMedia);
    Serial.print(F(",desiderata:"));
    Serial.print(durataDesiderata);
    Serial.print(F(",duty:"));
    Serial.print(duty*100);
    //Serial.print(F(",period:"));
    //Serial.print(pwmDeiPoveryTask.getInterval());
    Serial.println();
}

void lap() {
    //noInterrupts(); // non si può invocare da dentro, non ha effetto, va usata fuori per marcare sezioni critiche, qui dentro parte già a interrupt disabilitati

    //Serial.println("int!"); //da non fare...

    long m=millis();
    if((m-timestamp)>RUMORE) { // per eliminare gli interrupt spuri
        durata=m-timestamp;
        durataMedia=(durataMedia+durata)/2; // media mobile corta
        timestamp=m;
    }

    //interrupts(); // questa invece si può invocare (e in automatico vengono cmq riabilitati all'uscita), potrebbe avere senso riabilitarli anche prima di uscire se si deve solo fare qualcosa di non critico
}

void setup() {
    Serial.begin(115200);

    pinMode(SENSORE,INPUT_PULLUP);
    pinMode(ATTUATORE,OUTPUT);
    pinMode(LED_BUILTIN,OUTPUT);

    // CONFIGURAZIONE
    
    
    
    
    attachInterrupt(
        digitalPinToInterrupt(SENSORE),
        lap,
        FALLING);

    // Tasks
    runner.init();

    /*
    runner.addTask(blinkTask); // qui usare il task non la callback!
    blinkTask.enable();
    */

    runner.addTask(pwmDeiPoveryTask); // qui usare il task non la callback!
    pwmDeiPoveryTask.enable();

    runner.addTask(plotTask); // qui usare il task non la callback!
    plotTask.enable();
}

void loop() {
    runner.execute(); // TaskScheduler
}
