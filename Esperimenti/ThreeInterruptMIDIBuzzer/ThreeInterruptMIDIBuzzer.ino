/*
 * IL programma prende un interrupt per il cambio canzone dal pin 2 e sceglie se mandare avanti o indietro basandosi su quanto viene comunicato sul pin 4 (consultare fritzing per il cablaggio)
 * Il programma prende un interrupt per lo stato di play/pause dal pin 3
 * 
 * Le canzoni sono espresse come array di note, si possono aggiungere fino ad un massimo di 15 canzoni, semplicemente aggiungendo un vettore di note facilmente ottenibile utilizzando il sito
 * https://sparks.gogo.co.nz/midi_tone.html, e copiando/incollando la melodia all'interno di un nuovo vettore.
 * 
 * Assolutamente non è necessario copiare la parte di codice fornita dal sito, solo la parte del vettore di note è utile.
 * Ricordarsi di aggiornare la costante del numero di canzoni in memoria, presente nella sezione "Costanti e Variabili Globali" ad ogni variazione.
 */





////////////// INIZIO DEFINIZIONE DI OGNI SINGOLA NOTA ////////////////////////////////////////////////////////////


#include <avr/pgmspace.h>


// Octave 0 Note Codes
#define       NOTE_C_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00000000)
#define      NOTE_CS_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00000001)
#define       NOTE_D_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00000010)
#define      NOTE_DS_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00000011)
#define       NOTE_E_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00000100)
#define       NOTE_F_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00000101)
#define      NOTE_FS_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00000110)
#define       NOTE_G_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00000111)
#define      NOTE_GS_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00001000)
#define       NOTE_A_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00001001)
#define      NOTE_AS_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00001010)
#define       NOTE_B_0(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00001011)

// Octave 1 Note Codes
#define       NOTE_C_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00010000)
#define      NOTE_CS_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00010001)
#define       NOTE_D_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00010010)
#define      NOTE_DS_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00010011)
#define       NOTE_E_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00010100)
#define       NOTE_F_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00010101)
#define      NOTE_FS_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00010110)
#define       NOTE_G_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00010111)
#define      NOTE_GS_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00011000)
#define       NOTE_A_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00011001)
#define      NOTE_AS_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00011010)
#define       NOTE_B_1(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00011011)

// Octave 2 Note Codes
#define       NOTE_C_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00100000)
#define      NOTE_CS_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00100001)
#define       NOTE_D_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00100010)
#define      NOTE_DS_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00100011)
#define       NOTE_E_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00100100)
#define       NOTE_F_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00100101)
#define      NOTE_FS_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00100110)
#define       NOTE_G_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00100111)
#define      NOTE_GS_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00101000)
#define       NOTE_A_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00101001)
#define      NOTE_AS_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00101010)
#define       NOTE_B_2(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00101011)

// Octave 3 Note Codes
#define       NOTE_C_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00110000)
#define      NOTE_CS_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00110001)
#define       NOTE_D_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00110010)
#define      NOTE_DS_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00110011)
#define       NOTE_E_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00110100)
#define       NOTE_F_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00110101)
#define      NOTE_FS_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00110110)
#define       NOTE_G_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00110111)
#define      NOTE_GS_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00111000)
#define       NOTE_A_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00111001)
#define      NOTE_AS_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00111010)
#define       NOTE_B_3(DURATION) ( (((uint16_t)DURATION)<<8) | 0b00111011)

// Octave 4 Note Codes
#define       NOTE_C_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01000000)
#define      NOTE_CS_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01000001)
#define       NOTE_D_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01000010)
#define      NOTE_DS_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01000011)
#define       NOTE_E_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01000100)
#define       NOTE_F_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01000101)
#define      NOTE_FS_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01000110)
#define       NOTE_G_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01000111)
#define      NOTE_GS_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01001000)
#define       NOTE_A_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01001001)
#define      NOTE_AS_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01001010)
#define       NOTE_B_4(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01001011)

// Octave 5 Note Codes
#define       NOTE_C_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01010000)
#define      NOTE_CS_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01010001)
#define       NOTE_D_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01010010)
#define      NOTE_DS_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01010011)
#define       NOTE_E_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01010100)
#define       NOTE_F_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01010101)
#define      NOTE_FS_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01010110)
#define       NOTE_G_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01010111)
#define      NOTE_GS_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01011000)
#define       NOTE_A_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01011001)
#define      NOTE_AS_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01011010)
#define       NOTE_B_5(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01011011)

// Octave 6 Note Codes
#define       NOTE_C_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01100000)
#define      NOTE_CS_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01100001)
#define       NOTE_D_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01100010)
#define      NOTE_DS_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01100011)
#define       NOTE_E_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01100100)
#define       NOTE_F_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01100101)
#define      NOTE_FS_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01100110)
#define       NOTE_G_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01100111)
#define      NOTE_GS_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01101000)
#define       NOTE_A_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01101001)
#define      NOTE_AS_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01101010)
#define       NOTE_B_6(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01101011)

// Octave 7 Note Codes
#define       NOTE_C_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01110000)
#define      NOTE_CS_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01110001)
#define       NOTE_D_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01110010)
#define      NOTE_DS_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01110011)
#define       NOTE_E_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01110100)
#define       NOTE_F_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01110101)
#define      NOTE_FS_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01110110)
#define       NOTE_G_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01110111)
#define      NOTE_GS_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01111000)
#define       NOTE_A_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01111001)
#define      NOTE_AS_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01111010)
#define       NOTE_B_7(DURATION) ( (((uint16_t)DURATION)<<8) | 0b01111011)

// Octave 8 Note Codes
#define       NOTE_C_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10000000)
#define      NOTE_CS_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10000001)
#define       NOTE_D_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10000010)
#define      NOTE_DS_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10000011)
#define       NOTE_E_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10000100)
#define       NOTE_F_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10000101)
#define      NOTE_FS_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10000110)
#define       NOTE_G_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10000111)
#define      NOTE_GS_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10001000)
#define       NOTE_A_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10001001)
#define      NOTE_AS_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10001010)
#define       NOTE_B_8(DURATION) ( (((uint16_t)DURATION)<<8) | 0b10001011)

#define    NOTE_SILENT(DURATION) ((((uint16_t)DURATION)<<8) | 0b00001111)

////////////// FINE DEFINIZIONE DI OGNI SINGOLA NOTA ////////////////////////////////////////////////////////////


////////////// INIZIO ALLOCAZIONE VETTORI CANZONI ///////////////////////////////////////////////////////////////

// Track 0 - JJBA-PB - Sono Chi No Sadame
static const uint16_t Melody0[] PROGMEM = { 
           NOTE_AS_5( 120 ),     NOTE_G_5( 120 ),  NOTE_SILENT( 120 ),     NOTE_D_5( 120 ), 
            NOTE_F_5( 120 ),     NOTE_G_5( 120 ),    NOTE_AS_5( 120 ),     NOTE_A_5( 120 ), 
            NOTE_G_5( 120 ),  NOTE_SILENT( 120 ),     NOTE_D_5( 120 ),     NOTE_F_5( 120 ), 
           NOTE_AS_5( 120 ),     NOTE_A_5( 120 ),     NOTE_G_5( 120 ),     NOTE_F_5( 120 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 195 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 210 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 105 ),     NOTE_G_5( 120 ),  NOTE_SILENT( 120 ),     NOTE_D_5( 120 ), 
            NOTE_F_5( 120 ),     NOTE_G_5( 120 ),    NOTE_AS_5( 120 ),     NOTE_A_5( 120 ), 
            NOTE_G_5( 120 ),  NOTE_SILENT( 120 ),     NOTE_D_5( 120 ),     NOTE_F_5( 120 ), 
            NOTE_D_6( 120 ),     NOTE_C_6( 120 ),    NOTE_AS_5( 120 ),     NOTE_A_5( 120 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 195 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 225 ),  NOTE_SILENT( 240 ),     NOTE_C_6( 240 ), 
            NOTE_D_6( 240 ),     NOTE_F_6( 240 ),     NOTE_E_6( 180 ),     NOTE_C_6( 180 ), 
            NOTE_D_6( 120 ),     NOTE_C_6( 180 ),     NOTE_A_5( 180 ),    NOTE_AS_5( 120 ), 
            NOTE_A_5( 180 ),     NOTE_F_5( 180 ),     NOTE_G_5( 120 ),     NOTE_D_5( 255 ), 
            NOTE_D_5( 255 ),     NOTE_D_5( 255 ),     NOTE_D_5( 195 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_G_5( 120 ), 
            NOTE_A_5( 120 ),    NOTE_AS_5( 240 ),     NOTE_G_6( 240 ),     NOTE_F_6( 240 ), 
           NOTE_DS_6( 240 ),     NOTE_F_6( 240 ),  NOTE_SILENT( 120 ),     NOTE_F_6( 120 ), 
            NOTE_G_6( 120 ),     NOTE_D_6( 120 ),     NOTE_C_6( 120 ),    NOTE_AS_5( 120 ), 
            NOTE_C_6( 120 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),    NOTE_AS_5( 120 ), 
            NOTE_C_6( 120 ),  NOTE_SILENT( 120 ),     NOTE_C_6( 240 ),     NOTE_D_6( 240 ), 
            NOTE_G_6( 240 ),  NOTE_SILENT( 240 ),     NOTE_G_5( 120 ),     NOTE_A_5( 120 ), 
           NOTE_AS_5( 240 ),     NOTE_G_6( 240 ),     NOTE_F_6( 240 ),    NOTE_DS_6( 240 ), 
            NOTE_F_6( 240 ),  NOTE_SILENT( 120 ),     NOTE_F_6( 120 ),     NOTE_G_6( 120 ), 
            NOTE_D_6( 120 ),     NOTE_C_6( 120 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 195 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 225 ),     NOTE_F_5( 255 ),     NOTE_F_5( 225 ),     NOTE_G_5( 240 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
            NOTE_G_5(  60 ),     NOTE_A_5(  60 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 120 ), 
            NOTE_A_5( 120 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_D_6( 120 ), 
            NOTE_G_5( 180 ),     NOTE_A_5(  60 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 120 ), 
            NOTE_A_5( 120 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_F_6( 240 ), 
           NOTE_DS_6( 120 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 210 ), 
         NOTE_SILENT( 120 ),     NOTE_D_6(  60 ),     NOTE_E_6(  60 ),     NOTE_F_6( 120 ), 
            NOTE_G_6( 120 ),     NOTE_E_6( 120 ),     NOTE_F_6( 120 ),     NOTE_G_6( 120 ), 
            NOTE_A_6( 120 ),     NOTE_F_6( 120 ),     NOTE_E_6( 120 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 210 ),  NOTE_SILENT( 120 ),     NOTE_D_6(  60 ), 
            NOTE_E_6(  60 ),     NOTE_F_6( 120 ),     NOTE_G_6( 120 ),     NOTE_E_6( 120 ), 
            NOTE_F_6( 120 ),     NOTE_G_6( 120 ),     NOTE_A_6( 120 ),     NOTE_F_6( 120 ), 
            NOTE_E_6( 120 ),     NOTE_D_6( 255 ),     NOTE_D_6( 225 ),     NOTE_G_6( 240 ), 
         NOTE_SILENT( 240 ),     NOTE_G_6( 240 ),     NOTE_F_6( 120 ),    NOTE_DS_6( 120 ), 
            NOTE_F_6( 240 ),  NOTE_SILENT( 120 ),     NOTE_F_6( 120 ),     NOTE_E_6( 120 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  90 ),     NOTE_E_6( 240 ), 
           NOTE_FS_6( 255 ),    NOTE_FS_6( 255 ),    NOTE_FS_6( 255 ),    NOTE_FS_6( 255 ), 
           NOTE_FS_6( 255 ),    NOTE_FS_6( 165 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 225 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 225 ),    NOTE_AS_6( 240 ),     NOTE_A_6( 240 ), 
            NOTE_F_6( 240 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 210 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 225 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ), 
            NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 165 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 225 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),    NOTE_AS_5( 120 ),     NOTE_G_5( 120 ), 
         NOTE_SILENT( 120 ),     NOTE_D_5( 120 ),     NOTE_F_5( 120 ),     NOTE_G_5( 120 ), 
           NOTE_AS_5( 120 ),     NOTE_A_5( 120 ),     NOTE_G_5( 120 ),  NOTE_SILENT( 120 ), 
            NOTE_D_5( 120 ),     NOTE_F_5( 120 ),    NOTE_AS_5( 120 ),     NOTE_A_5( 120 ), 
            NOTE_G_5( 120 ),     NOTE_F_5( 120 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 195 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 210 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 105 ),     NOTE_G_5( 120 ), 
         NOTE_SILENT( 120 ),     NOTE_D_5( 120 ),     NOTE_F_5( 120 ),     NOTE_G_5( 120 ), 
           NOTE_AS_5( 120 ),     NOTE_A_5( 120 ),     NOTE_G_5( 120 ),  NOTE_SILENT( 120 ), 
            NOTE_D_5( 120 ),     NOTE_F_5( 120 ),     NOTE_D_6( 120 ),     NOTE_C_6( 120 ), 
           NOTE_AS_5( 120 ),     NOTE_A_5( 120 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 195 ),     NOTE_C_6( 255 ),     NOTE_C_6( 225 ), 
         NOTE_SILENT( 240 ),     NOTE_C_6( 240 ),     NOTE_D_6( 240 ),     NOTE_F_6( 240 ), 
            NOTE_E_6( 180 ),     NOTE_C_6( 180 ),     NOTE_D_6( 120 ),     NOTE_C_6( 180 ), 
            NOTE_A_5( 180 ),    NOTE_AS_5( 120 ),     NOTE_A_5( 180 ),     NOTE_F_5( 180 ), 
            NOTE_G_5( 120 ),     NOTE_D_5( 255 ),     NOTE_D_5( 255 ),     NOTE_D_5( 255 ), 
            NOTE_D_5( 195 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),     NOTE_G_5( 120 ),     NOTE_A_5( 120 ),    NOTE_AS_5( 240 ), 
            NOTE_G_6( 240 ),     NOTE_F_6( 240 ),    NOTE_DS_6( 240 ),     NOTE_F_6( 240 ), 
         NOTE_SILENT( 120 ),     NOTE_F_6( 120 ),     NOTE_G_6( 120 ),     NOTE_D_6( 120 ), 
            NOTE_C_6( 120 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),    NOTE_AS_5( 120 ), 
            NOTE_C_6( 120 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),  NOTE_SILENT( 120 ), 
            NOTE_C_6( 240 ),     NOTE_D_6( 240 ),     NOTE_G_6( 240 ),  NOTE_SILENT( 240 ), 
            NOTE_G_5( 120 ),     NOTE_A_5( 120 ),    NOTE_AS_5( 240 ),     NOTE_G_6( 240 ), 
            NOTE_F_6( 240 ),    NOTE_DS_6( 240 ),     NOTE_F_6( 240 ),  NOTE_SILENT( 120 ), 
            NOTE_F_6( 120 ),     NOTE_G_6( 120 ),     NOTE_D_6( 120 ),     NOTE_C_6( 120 ), 
           NOTE_AS_5( 120 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 195 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 225 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 225 ),     NOTE_G_5( 240 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_G_5(  60 ),     NOTE_A_5(  60 ), 
           NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_A_5( 120 ),    NOTE_AS_5( 120 ), 
            NOTE_C_6( 120 ),     NOTE_D_6( 120 ),     NOTE_G_5( 180 ),     NOTE_A_5(  60 ), 
           NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_A_5( 120 ),    NOTE_AS_5( 120 ), 
            NOTE_C_6( 120 ),     NOTE_F_6( 240 ),    NOTE_DS_6( 120 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 210 ),  NOTE_SILENT( 120 ),     NOTE_D_6(  60 ), 
            NOTE_E_6(  60 ),     NOTE_F_6( 120 ),     NOTE_G_6( 120 ),     NOTE_E_6( 120 ), 
            NOTE_F_6( 120 ),     NOTE_G_6( 120 ),     NOTE_A_6( 120 ),     NOTE_F_6( 120 ), 
            NOTE_E_6( 120 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 210 ), 
         NOTE_SILENT( 120 ),     NOTE_D_6(  60 ),     NOTE_E_6(  60 ),     NOTE_F_6( 120 ), 
            NOTE_G_6( 120 ),     NOTE_E_6( 120 ),     NOTE_F_6( 120 ),     NOTE_G_6( 120 ), 
            NOTE_A_6( 120 ),     NOTE_F_6( 120 ),     NOTE_E_6( 120 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 225 ),     NOTE_G_6( 240 ),  NOTE_SILENT( 240 ),     NOTE_G_6( 240 ), 
            NOTE_F_6( 120 ),    NOTE_DS_6( 120 ),     NOTE_F_6( 240 ),  NOTE_SILENT( 120 ), 
            NOTE_F_6( 120 ),     NOTE_E_6( 120 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6(  90 ),     NOTE_E_6( 240 ),    NOTE_FS_6( 255 ),    NOTE_FS_6( 255 ), 
           NOTE_FS_6( 255 ),    NOTE_FS_6( 255 ),    NOTE_FS_6( 255 ),    NOTE_FS_6( 165 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 225 ),     NOTE_G_6( 255 ),     NOTE_G_6( 225 ), 
           NOTE_AS_6( 240 ),     NOTE_A_6( 240 ),     NOTE_F_6( 240 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 210 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 225 ), 
            NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ), 
            NOTE_A_6( 255 ),     NOTE_A_6( 165 ),     NOTE_G_6( 255 ),     NOTE_G_6( 225 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 225 ),     NOTE_F_5( 240 ),     NOTE_G_5( 240 ), 
            NOTE_A_5( 255 ),     NOTE_A_5( 225 ),     NOTE_G_5( 240 ),     NOTE_A_5( 240 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 225 ),     NOTE_A_5( 240 ),    NOTE_AS_5( 240 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 225 ),    NOTE_AS_5( 240 ),     NOTE_C_6( 240 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 195 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 195 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 225 ),     NOTE_C_6( 240 ),     NOTE_D_6( 240 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 225 ),     NOTE_D_6( 240 ),     NOTE_E_6( 240 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 225 ),     NOTE_E_6( 240 ),     NOTE_F_6( 240 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 225 ),     NOTE_F_6( 240 ),     NOTE_G_6( 240 ), 
            NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ), 
            NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 150 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_G_5(  60 ), 
            NOTE_A_5(  60 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_A_5( 120 ), 
           NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_D_6( 120 ),     NOTE_G_5( 180 ), 
            NOTE_A_5(  60 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_A_5( 120 ), 
           NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_F_6( 240 ),    NOTE_DS_6( 120 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 210 ),  NOTE_SILENT( 120 ), 
            NOTE_D_6(  60 ),     NOTE_E_6(  60 ),     NOTE_F_6( 120 ),     NOTE_G_6( 120 ), 
            NOTE_E_6( 120 ),     NOTE_F_6( 120 ),     NOTE_G_6( 120 ),     NOTE_A_6( 120 ), 
            NOTE_F_6( 120 ),     NOTE_E_6( 120 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 210 ),  NOTE_SILENT( 120 ),     NOTE_D_6(  60 ),     NOTE_E_6(  60 ), 
            NOTE_F_6( 120 ),     NOTE_G_6( 120 ),     NOTE_E_6( 120 ),     NOTE_F_6( 120 ), 
            NOTE_G_6( 120 ),     NOTE_A_6( 120 ),     NOTE_F_6( 120 ),     NOTE_E_6( 120 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 225 ),     NOTE_G_6( 240 ),  NOTE_SILENT( 240 ), 
            NOTE_G_6( 240 ),     NOTE_F_6( 120 ),    NOTE_DS_6( 120 ),     NOTE_F_6( 240 ), 
         NOTE_SILENT( 120 ),     NOTE_F_6( 120 ),     NOTE_E_6( 120 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6(  90 ),     NOTE_E_6( 240 ),    NOTE_FS_6( 255 ), 
           NOTE_FS_6( 225 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_G_5(  60 ), 
            NOTE_A_5(  60 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_A_5( 120 ), 
           NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_D_6( 120 ),     NOTE_G_5( 180 ), 
            NOTE_A_5(  60 ),    NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_A_5( 120 ), 
           NOTE_AS_5( 120 ),     NOTE_C_6( 120 ),     NOTE_F_6( 240 ),    NOTE_DS_6( 120 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 210 ),  NOTE_SILENT( 120 ), 
            NOTE_D_6(  60 ),     NOTE_E_6(  60 ),     NOTE_F_6( 120 ),     NOTE_G_6( 120 ), 
            NOTE_E_6( 120 ),     NOTE_F_6( 120 ),     NOTE_G_6( 120 ),     NOTE_A_6( 120 ), 
            NOTE_F_6( 120 ),     NOTE_E_6( 120 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 210 ),  NOTE_SILENT( 120 ),     NOTE_D_6(  60 ),     NOTE_E_6(  60 ), 
            NOTE_F_6( 120 ),     NOTE_G_6( 120 ),     NOTE_E_6( 120 ),     NOTE_F_6( 120 ), 
            NOTE_G_6( 120 ),     NOTE_A_6( 120 ),     NOTE_F_6( 120 ),     NOTE_E_6( 120 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 225 ),     NOTE_G_6( 240 ),  NOTE_SILENT( 240 ), 
            NOTE_G_6( 240 ),     NOTE_F_6( 120 ),    NOTE_DS_6( 120 ),     NOTE_F_6( 240 ), 
         NOTE_SILENT( 120 ),     NOTE_F_6( 120 ),     NOTE_E_6( 120 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6(  90 ),     NOTE_E_6( 240 ),    NOTE_FS_6( 255 ), 
           NOTE_FS_6( 255 ),    NOTE_FS_6( 255 ),    NOTE_FS_6( 255 ),    NOTE_FS_6( 255 ), 
           NOTE_FS_6( 165 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 225 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 225 ),    NOTE_AS_6( 240 ),     NOTE_A_6( 240 ),     NOTE_F_6( 240 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 210 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 225 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ), 
            NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ), 
            NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ), 
            NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6(  45 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 225 ), 
  };
static const uint16_t Melody0_Length    = sizeof( Melody0 ) / sizeof(uint16_t);



//Track 1 - JJBA-BT - Bloody Stream
static const uint16_t Melody1[] PROGMEM = { 
 
            NOTE_C_6( 119 ),    NOTE_DS_6( 119 ),     NOTE_G_6( 119 ),  NOTE_SILENT( 121 ), 
           NOTE_FS_6( 239 ),     NOTE_C_6( 119 ),     NOTE_F_6( 119 ),  NOTE_SILENT( 121 ), 
            NOTE_F_6( 119 ),  NOTE_SILENT( 121 ),     NOTE_C_6( 119 ),    NOTE_DS_6( 239 ), 
            NOTE_F_6( 239 ),     NOTE_C_6( 119 ),    NOTE_DS_6( 119 ),     NOTE_G_6( 119 ), 
         NOTE_SILENT( 121 ),    NOTE_FS_6( 239 ),     NOTE_C_6( 119 ),     NOTE_F_6( 119 ), 
         NOTE_SILENT( 121 ),     NOTE_F_6( 119 ),  NOTE_SILENT( 121 ),     NOTE_C_6( 119 ), 
           NOTE_DS_6( 239 ),     NOTE_F_6( 239 ),     NOTE_C_6( 119 ),    NOTE_DS_6( 119 ), 
            NOTE_G_6( 119 ),  NOTE_SILENT( 121 ),    NOTE_FS_6( 239 ),     NOTE_C_6( 119 ), 
            NOTE_F_6( 119 ),  NOTE_SILENT( 121 ),     NOTE_F_6( 119 ),  NOTE_SILENT( 121 ), 
            NOTE_C_6( 119 ),    NOTE_DS_6( 239 ),     NOTE_F_6( 239 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 194 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 193 ),    NOTE_FS_5( 255 ), 
           NOTE_FS_5( 255 ),    NOTE_FS_5( 209 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 179 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 196 ),    NOTE_DS_5( 255 ), 
           NOTE_DS_5( 224 ),     NOTE_F_5( 255 ),     NOTE_F_5( 224 ),    NOTE_FS_5( 255 ), 
           NOTE_FS_5( 255 ),    NOTE_FS_5( 209 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 194 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_G_5( 239 ),     NOTE_C_6( 239 ), 
            NOTE_G_5( 239 ),     NOTE_F_5( 239 ),    NOTE_DS_5( 119 ),     NOTE_F_5( 239 ), 
           NOTE_DS_5( 239 ),     NOTE_C_5( 119 ),    NOTE_DS_5( 239 ),     NOTE_C_5( 119 ), 
           NOTE_DS_5( 255 ),    NOTE_DS_5( 255 ),    NOTE_DS_5(  88 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 211 ),    NOTE_DS_5( 239 ),     NOTE_F_5( 239 ), 
           NOTE_DS_5( 239 ),    NOTE_FS_5( 255 ),    NOTE_FS_5( 255 ),    NOTE_FS_5( 209 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 194 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 193 ), 
           NOTE_FS_5( 255 ),    NOTE_FS_5( 255 ),    NOTE_FS_5( 209 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 179 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 196 ), 
           NOTE_DS_5( 255 ),    NOTE_DS_5( 224 ),     NOTE_F_5( 255 ),     NOTE_F_5( 224 ), 
           NOTE_FS_5( 255 ),    NOTE_FS_5( 255 ),    NOTE_FS_5( 209 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 179 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 224 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 208 ),     NOTE_D_6( 119 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 104 ), 
            NOTE_D_6( 239 ),     NOTE_C_6( 239 ),     NOTE_D_6( 119 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6(  89 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 211 ),    NOTE_DS_5( 255 ),    NOTE_DS_5( 255 ),    NOTE_DS_5( 207 ), 
           NOTE_AS_5( 239 ),    NOTE_GS_5( 119 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 194 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 207 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 208 ), 
           NOTE_AS_6( 119 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 103 ),     NOTE_D_7( 255 ), 
            NOTE_D_7( 104 ),    NOTE_DS_7( 239 ),     NOTE_D_7( 255 ),     NOTE_D_7( 255 ), 
            NOTE_D_7( 255 ),     NOTE_D_7( 255 ),     NOTE_D_7( 179 ),    NOTE_AS_6( 255 ), 
           NOTE_AS_6( 223 ),     NOTE_C_7( 119 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6(  59 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 211 ),    NOTE_DS_6( 239 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 224 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 194 ),     NOTE_C_6( 239 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 179 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 104 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 104 ),    NOTE_AS_6( 239 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 134 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 224 ),     NOTE_B_5( 255 ),     NOTE_B_5( 224 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 224 ),    NOTE_DS_6( 239 ),     NOTE_D_6( 239 ), 
            NOTE_C_6( 119 ),    NOTE_AS_5( 239 ),     NOTE_C_6( 119 ),  NOTE_SILENT( 241 ), 
            NOTE_G_5( 239 ),     NOTE_C_6( 239 ),    NOTE_DS_6( 239 ),     NOTE_G_6( 239 ), 
            NOTE_F_6( 239 ),    NOTE_DS_6( 119 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 211 ),    NOTE_AS_5( 238 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 110 ),     NOTE_B_5( 255 ),     NOTE_B_5( 103 ),     NOTE_C_6( 239 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6(  59 ),    NOTE_DS_6( 239 ),     NOTE_D_6( 239 ),     NOTE_C_6( 119 ), 
           NOTE_AS_5( 239 ),     NOTE_C_6( 119 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 239 ), 
            NOTE_C_6( 239 ),    NOTE_DS_6( 239 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 209 ),    NOTE_AS_5( 239 ),    NOTE_GS_6( 239 ),     NOTE_G_6( 239 ), 
            NOTE_F_6( 239 ),     NOTE_G_6( 239 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 209 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 179 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),    NOTE_DS_6( 239 ),     NOTE_D_6( 239 ), 
            NOTE_C_6( 119 ),    NOTE_AS_5( 239 ),     NOTE_C_6( 119 ),  NOTE_SILENT( 241 ), 
            NOTE_G_5( 239 ),     NOTE_C_6( 239 ),    NOTE_DS_6( 239 ),     NOTE_G_6( 239 ), 
            NOTE_F_6( 239 ),    NOTE_DS_6( 119 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 211 ),    NOTE_AS_5( 238 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 110 ),     NOTE_B_5( 255 ),     NOTE_B_5( 103 ),     NOTE_C_6( 239 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6(  59 ),    NOTE_DS_6( 239 ),     NOTE_D_6( 239 ),     NOTE_C_6( 119 ), 
           NOTE_AS_5( 239 ),     NOTE_C_6( 119 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 239 ), 
            NOTE_C_6( 239 ),    NOTE_DS_6( 239 ),     NOTE_G_6( 239 ),     NOTE_F_6( 239 ), 
           NOTE_DS_6( 119 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ),  NOTE_SILENT( 241 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 223 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 149 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 226 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 194 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 224 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ),     NOTE_D_6( 238 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  29 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 194 ),    NOTE_AS_6( 119 ), 
            NOTE_C_7( 119 ),    NOTE_AS_6( 239 ),    NOTE_DS_6( 119 ),     NOTE_C_6( 119 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 224 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_G_6( 119 ),    NOTE_GS_6( 119 ), 
           NOTE_AS_6( 119 ),     NOTE_G_6( 255 ),     NOTE_G_6( 104 ),     NOTE_F_6( 119 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 104 ),     NOTE_F_6( 239 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 224 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 255 ), 
           NOTE_AS_6( 255 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 255 ), 
           NOTE_AS_6( 134 ),     NOTE_C_6( 119 ),    NOTE_DS_6( 119 ),     NOTE_G_6( 119 ), 
         NOTE_SILENT( 121 ),    NOTE_FS_6( 239 ),     NOTE_C_6( 119 ),     NOTE_F_6( 119 ), 
         NOTE_SILENT( 121 ),     NOTE_F_6( 119 ),  NOTE_SILENT( 121 ),     NOTE_C_6( 119 ), 
           NOTE_DS_6( 239 ),     NOTE_F_6( 239 ),     NOTE_C_6( 119 ),    NOTE_DS_6( 119 ), 
            NOTE_G_6( 119 ),  NOTE_SILENT( 121 ),    NOTE_FS_6( 239 ),     NOTE_C_6( 119 ), 
            NOTE_F_6( 119 ),  NOTE_SILENT( 121 ),     NOTE_F_6( 119 ),  NOTE_SILENT( 121 ), 
            NOTE_C_6( 119 ),    NOTE_DS_6( 239 ),     NOTE_F_6( 239 ),     NOTE_C_6( 119 ), 
           NOTE_DS_6( 119 ),     NOTE_G_6( 119 ),  NOTE_SILENT( 121 ),    NOTE_FS_6( 239 ), 
            NOTE_C_6( 119 ),     NOTE_F_6( 119 ),  NOTE_SILENT( 121 ),     NOTE_F_6( 119 ), 
         NOTE_SILENT( 121 ),     NOTE_C_6( 119 ),    NOTE_DS_6( 239 ),     NOTE_F_6( 239 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 194 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 193 ), 
           NOTE_FS_5( 255 ),    NOTE_FS_5( 255 ),    NOTE_FS_5( 209 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 179 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 196 ), 
           NOTE_DS_5( 255 ),    NOTE_DS_5( 224 ),     NOTE_F_5( 255 ),     NOTE_F_5( 224 ), 
           NOTE_FS_5( 255 ),    NOTE_FS_5( 255 ),    NOTE_FS_5( 209 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 194 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_G_5( 239 ), 
            NOTE_C_6( 239 ),     NOTE_G_5( 239 ),     NOTE_F_5( 239 ),    NOTE_DS_5( 119 ), 
            NOTE_F_5( 239 ),    NOTE_DS_5( 239 ),     NOTE_C_5( 119 ),    NOTE_DS_5( 239 ), 
            NOTE_C_5( 119 ),    NOTE_DS_5( 255 ),    NOTE_DS_5( 255 ),    NOTE_DS_5(  88 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 211 ),    NOTE_DS_5( 239 ), 
            NOTE_F_5( 239 ),    NOTE_DS_5( 239 ),    NOTE_FS_5( 255 ),    NOTE_FS_5( 255 ), 
           NOTE_FS_5( 209 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 194 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 193 ),    NOTE_FS_5( 255 ),    NOTE_FS_5( 255 ),    NOTE_FS_5( 209 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 179 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 196 ),    NOTE_DS_5( 255 ),    NOTE_DS_5( 224 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 224 ),    NOTE_FS_5( 255 ),    NOTE_FS_5( 255 ),    NOTE_FS_5( 209 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 179 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 224 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 208 ),     NOTE_D_6( 119 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 104 ),     NOTE_D_6( 239 ),     NOTE_C_6( 239 ),     NOTE_D_6( 119 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  89 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 211 ),    NOTE_DS_5( 255 ),    NOTE_DS_5( 255 ), 
           NOTE_DS_5( 207 ),    NOTE_AS_5( 239 ),    NOTE_GS_5( 119 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 194 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 207 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 208 ),    NOTE_AS_6( 119 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 103 ), 
            NOTE_D_7( 255 ),     NOTE_D_7( 104 ),    NOTE_DS_7( 239 ),     NOTE_D_7( 255 ), 
            NOTE_D_7( 255 ),     NOTE_D_7( 255 ),     NOTE_D_7( 255 ),     NOTE_D_7( 179 ), 
           NOTE_AS_6( 255 ),    NOTE_AS_6( 223 ),     NOTE_C_7( 119 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6(  59 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 211 ),    NOTE_DS_6( 239 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 224 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 194 ),     NOTE_C_6( 239 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 179 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 104 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 104 ), 
           NOTE_AS_6( 239 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 134 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 224 ),     NOTE_B_5( 255 ), 
            NOTE_B_5( 224 ),     NOTE_D_6( 255 ),     NOTE_D_6( 224 ),    NOTE_DS_6( 239 ), 
            NOTE_D_6( 239 ),     NOTE_C_6( 119 ),    NOTE_AS_5( 239 ),     NOTE_C_6( 119 ), 
         NOTE_SILENT( 241 ),     NOTE_G_5( 239 ),     NOTE_C_6( 239 ),    NOTE_DS_6( 239 ), 
            NOTE_G_6( 239 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ),     NOTE_F_6( 239 ), 
           NOTE_DS_6( 119 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 211 ), 
           NOTE_AS_5( 238 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 110 ),     NOTE_B_5( 255 ),     NOTE_B_5( 103 ), 
            NOTE_C_6( 239 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6(  59 ),    NOTE_DS_6( 239 ),     NOTE_D_6( 239 ), 
            NOTE_C_6( 119 ),    NOTE_AS_5( 239 ),     NOTE_C_6( 119 ),  NOTE_SILENT( 241 ), 
            NOTE_G_5( 239 ),     NOTE_C_6( 239 ),    NOTE_DS_6( 239 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 209 ),    NOTE_AS_5( 239 ),    NOTE_GS_6( 239 ), 
            NOTE_G_6( 239 ),     NOTE_F_6( 239 ),     NOTE_G_6( 239 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 209 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 179 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),    NOTE_DS_6( 239 ), 
            NOTE_D_6( 239 ),     NOTE_C_6( 119 ),    NOTE_AS_5( 239 ),     NOTE_C_6( 119 ), 
         NOTE_SILENT( 241 ),     NOTE_G_5( 239 ),     NOTE_C_6( 239 ),    NOTE_DS_6( 239 ), 
            NOTE_G_6( 239 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ),     NOTE_F_6( 239 ), 
           NOTE_DS_6( 119 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 211 ), 
           NOTE_AS_5( 238 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 110 ),     NOTE_B_5( 255 ),     NOTE_B_5( 103 ), 
            NOTE_C_6( 239 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6(  59 ),    NOTE_DS_6( 239 ),     NOTE_D_6( 239 ), 
            NOTE_C_6( 119 ),    NOTE_AS_5( 239 ),     NOTE_C_6( 119 ),  NOTE_SILENT( 241 ), 
            NOTE_G_5( 239 ),     NOTE_C_6( 239 ),    NOTE_DS_6( 239 ),     NOTE_G_6( 239 ), 
            NOTE_F_6( 239 ),    NOTE_DS_6( 119 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ), 
         NOTE_SILENT( 241 ),     NOTE_G_5( 255 ),     NOTE_G_5( 223 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 149 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 226 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 194 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 224 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ), 
            NOTE_D_6( 238 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 149 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6(  43 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 103 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 194 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 194 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 226 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 208 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 209 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 134 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 134 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 224 ),     NOTE_B_5( 255 ),     NOTE_B_5( 224 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 224 ),    NOTE_DS_6( 239 ),     NOTE_D_6( 239 ), 
            NOTE_C_6( 119 ),    NOTE_AS_5( 239 ),     NOTE_C_6( 119 ),  NOTE_SILENT( 241 ), 
            NOTE_G_5( 239 ),     NOTE_C_6( 239 ),    NOTE_DS_6( 239 ),     NOTE_G_6( 239 ), 
            NOTE_F_6( 239 ),    NOTE_DS_6( 119 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 211 ),    NOTE_AS_5( 238 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 110 ),     NOTE_B_5( 255 ),     NOTE_B_5( 103 ),     NOTE_C_6( 239 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6(  59 ),    NOTE_DS_6( 239 ),     NOTE_D_6( 239 ),     NOTE_C_6( 119 ), 
           NOTE_AS_5( 239 ),     NOTE_C_6( 119 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 239 ), 
            NOTE_C_6( 239 ),    NOTE_DS_6( 239 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 209 ),    NOTE_AS_5( 239 ),    NOTE_GS_6( 239 ),     NOTE_G_6( 239 ), 
            NOTE_F_6( 239 ),     NOTE_G_6( 239 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 209 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 179 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),    NOTE_DS_6( 239 ),     NOTE_D_6( 239 ), 
            NOTE_C_6( 119 ),    NOTE_AS_5( 239 ),     NOTE_C_6( 119 ),  NOTE_SILENT( 241 ), 
            NOTE_G_5( 239 ),     NOTE_C_6( 239 ),    NOTE_DS_6( 239 ),     NOTE_G_6( 239 ), 
            NOTE_F_6( 239 ),    NOTE_DS_6( 119 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 211 ),    NOTE_AS_5( 238 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 110 ),     NOTE_B_5( 255 ),     NOTE_B_5( 103 ),     NOTE_C_6( 239 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6(  59 ),    NOTE_DS_6( 239 ),     NOTE_D_6( 239 ),     NOTE_C_6( 119 ), 
           NOTE_AS_5( 239 ),     NOTE_C_6( 119 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 239 ), 
            NOTE_C_6( 239 ),    NOTE_DS_6( 239 ),     NOTE_G_6( 239 ),     NOTE_F_6( 239 ), 
           NOTE_DS_6( 119 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ),  NOTE_SILENT( 241 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 223 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 148 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 226 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 194 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 224 ),     NOTE_F_6( 239 ),    NOTE_DS_6( 119 ),     NOTE_D_6( 238 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6(  42 ), 
  };
static const uint16_t Melody1_Length    = sizeof( Melody1 ) / sizeof(uint16_t);

// Track 2 - JJBA-SC - Noriaki Kakioin Theme: Virtuous Pope
static const uint16_t Melody2[] PROGMEM = { 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5(  56 ),  NOTE_SILENT(  97 ), 
           NOTE_GS_5(  77 ),     NOTE_A_5(  77 ),  NOTE_SILENT(   2 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 182 ),  NOTE_SILENT( 186 ),    NOTE_GS_5(  77 ),     NOTE_G_5(  77 ), 
         NOTE_SILENT(   2 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 236 ),  NOTE_SILENT( 162 ),     NOTE_F_5(  44 ),  NOTE_SILENT(   4 ), 
           NOTE_FS_5(  44 ),  NOTE_SILENT(   4 ),     NOTE_G_5(  44 ),  NOTE_SILENT(   4 ), 
           NOTE_GS_5(  44 ),  NOTE_SILENT(   4 ),     NOTE_A_5(  44 ),  NOTE_SILENT(   4 ), 
           NOTE_AS_5(  44 ),  NOTE_SILENT(   4 ),     NOTE_B_5(  44 ),  NOTE_SILENT(   4 ), 
            NOTE_C_6(  44 ),  NOTE_SILENT(   4 ),    NOTE_CS_6(  44 ),  NOTE_SILENT(   4 ), 
            NOTE_D_6(  44 ),  NOTE_SILENT(   4 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6(  38 ),  NOTE_SILENT(  97 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6(  92 ),  NOTE_SILENT(  73 ),     NOTE_G_5( 119 ),    NOTE_AS_5( 119 ), 
            NOTE_C_6( 227 ),  NOTE_SILENT(  13 ),     NOTE_D_6( 239 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 226 ),     NOTE_G_5( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 227 ), 
         NOTE_SILENT(  13 ),     NOTE_D_6( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 122 ), 
         NOTE_SILENT( 241 ),     NOTE_D_6( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 238 ), 
           NOTE_AS_5( 119 ),     NOTE_C_6( 113 ),  NOTE_SILENT(   7 ),     NOTE_D_6( 239 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 226 ),     NOTE_G_5( 122 ),  NOTE_SILENT( 241 ), 
            NOTE_G_5( 227 ),  NOTE_SILENT(  13 ),     NOTE_D_6( 122 ),  NOTE_SILENT( 241 ), 
            NOTE_G_5( 227 ),  NOTE_SILENT( 253 ),     NOTE_D_6( 122 ),  NOTE_SILENT( 241 ), 
            NOTE_G_5( 119 ),    NOTE_AS_5( 119 ),     NOTE_C_6( 119 ),     NOTE_D_6( 113 ), 
         NOTE_SILENT(   7 ),     NOTE_C_6( 239 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 226 ), 
            NOTE_G_5( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 227 ),  NOTE_SILENT(  13 ), 
            NOTE_C_6( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 227 ),  NOTE_SILENT( 253 ), 
            NOTE_C_6( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 238 ),    NOTE_AS_5( 119 ), 
            NOTE_C_6( 113 ),  NOTE_SILENT(   7 ),     NOTE_D_6( 239 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 226 ),     NOTE_G_5( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 227 ), 
         NOTE_SILENT(  13 ),     NOTE_D_6( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_5( 227 ), 
         NOTE_SILENT( 253 ),     NOTE_D_7( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_6( 238 ), 
           NOTE_AS_6( 119 ),     NOTE_C_7( 113 ),  NOTE_SILENT(   7 ),     NOTE_D_7( 239 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 226 ),     NOTE_G_6( 122 ),  NOTE_SILENT( 241 ), 
            NOTE_G_6( 227 ),  NOTE_SILENT(  13 ),     NOTE_D_7( 122 ),  NOTE_SILENT( 241 ), 
            NOTE_G_6( 227 ),  NOTE_SILENT( 253 ),     NOTE_D_7( 122 ),  NOTE_SILENT( 241 ), 
            NOTE_G_6( 238 ),    NOTE_AS_6( 119 ),     NOTE_C_7( 113 ),  NOTE_SILENT(   7 ), 
            NOTE_D_7( 239 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 226 ),     NOTE_G_6( 122 ), 
         NOTE_SILENT( 241 ),     NOTE_G_6( 227 ),  NOTE_SILENT(  13 ),     NOTE_D_7( 122 ), 
         NOTE_SILENT( 241 ),     NOTE_G_6( 227 ),  NOTE_SILENT( 253 ),     NOTE_D_7( 122 ), 
         NOTE_SILENT( 241 ),     NOTE_G_6( 119 ),    NOTE_AS_6( 119 ),     NOTE_C_7( 119 ), 
            NOTE_D_7( 113 ),  NOTE_SILENT(   7 ),     NOTE_C_7( 239 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 226 ),     NOTE_G_6( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_6( 227 ), 
         NOTE_SILENT(  13 ),     NOTE_C_7( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_6( 227 ), 
         NOTE_SILENT( 253 ),     NOTE_C_7( 122 ),  NOTE_SILENT( 241 ),     NOTE_G_6( 238 ), 
           NOTE_AS_6( 119 ),     NOTE_C_7( 113 ),  NOTE_SILENT(   7 ),     NOTE_D_7( 239 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 226 ),     NOTE_G_6( 122 ),  NOTE_SILENT( 241 ), 
            NOTE_G_6( 227 ),  NOTE_SILENT(  13 ),     NOTE_D_7( 122 ),  NOTE_SILENT( 241 ), 
            NOTE_G_6( 227 ),  NOTE_SILENT( 253 ),     NOTE_F_7( 122 ),  NOTE_SILENT( 241 ), 
            NOTE_C_5(  29 ),     NOTE_D_5(  29 ),    NOTE_DS_5(  29 ),     NOTE_F_5(  23 ), 
         NOTE_SILENT(   7 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 185 ), 
         NOTE_SILENT( 145 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 200 ),  NOTE_SILENT(  25 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 200 ),  NOTE_SILENT(  25 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 185 ),  NOTE_SILENT( 145 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 200 ),  NOTE_SILENT(  25 ),     NOTE_D_6( 255 ),     NOTE_D_6( 200 ), 
         NOTE_SILENT(  25 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 146 ),  NOTE_SILENT(  49 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5(  38 ),  NOTE_SILENT(  97 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 200 ),  NOTE_SILENT(  25 ),     NOTE_A_5( 255 ),     NOTE_A_5( 200 ), 
         NOTE_SILENT(  25 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5(  38 ),  NOTE_SILENT(  97 ),     NOTE_A_5(  77 ),     NOTE_G_5(  77 ), 
         NOTE_SILENT(   2 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 251 ), 
         NOTE_SILENT(  42 ),     NOTE_D_5( 255 ),     NOTE_D_5( 255 ),     NOTE_D_5( 255 ), 
            NOTE_D_5( 146 ),  NOTE_SILENT(  49 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 185 ),  NOTE_SILENT( 145 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 200 ), 
         NOTE_SILENT(  25 ),     NOTE_C_6( 255 ),     NOTE_C_6( 200 ),  NOTE_SILENT(  25 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 185 ),  NOTE_SILENT( 145 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 200 ),  NOTE_SILENT(  25 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 200 ),  NOTE_SILENT(  25 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 146 ),  NOTE_SILENT(  49 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5(  38 ),  NOTE_SILENT(  97 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 200 ),  NOTE_SILENT(  25 ),     NOTE_A_5( 255 ), 
            NOTE_A_5( 200 ),  NOTE_SILENT(  25 ),    NOTE_AS_5( 255 ),    NOTE_AS_5(  86 ), 
         NOTE_SILENT(  19 ),     NOTE_G_5( 255 ),     NOTE_G_5(  86 ),  NOTE_SILENT(  19 ), 
           NOTE_AS_5( 227 ),  NOTE_SILENT(  13 ),     NOTE_D_6( 255 ),     NOTE_D_6(  86 ), 
         NOTE_SILENT(  19 ),    NOTE_AS_5( 255 ),    NOTE_AS_5(  86 ),  NOTE_SILENT(  19 ), 
            NOTE_D_6( 227 ),  NOTE_SILENT(  13 ),     NOTE_G_6( 255 ),     NOTE_G_6(  86 ), 
         NOTE_SILENT(  19 ),     NOTE_D_6( 255 ),     NOTE_D_6(  86 ),  NOTE_SILENT(  19 ), 
            NOTE_G_6( 227 ),  NOTE_SILENT(  13 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ), 
            NOTE_A_6( 255 ),     NOTE_A_6( 146 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 185 ),  NOTE_SILENT( 145 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 200 ), 
         NOTE_SILENT(  25 ),     NOTE_C_6( 255 ),     NOTE_C_6( 200 ),  NOTE_SILENT(  25 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 185 ),  NOTE_SILENT( 145 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 200 ),  NOTE_SILENT(  25 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 200 ),  NOTE_SILENT(  25 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 146 ),  NOTE_SILENT(  49 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5(  38 ),  NOTE_SILENT(  97 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 200 ),  NOTE_SILENT(  25 ),     NOTE_A_5( 255 ), 
            NOTE_A_5( 200 ),  NOTE_SILENT(  25 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5(  38 ),  NOTE_SILENT(  97 ),     NOTE_A_5(  77 ), 
            NOTE_G_5(  77 ),  NOTE_SILENT(   2 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 251 ),  NOTE_SILENT(  42 ),     NOTE_D_5( 255 ),     NOTE_D_5( 255 ), 
            NOTE_D_5( 255 ),     NOTE_D_5( 146 ),  NOTE_SILENT(  49 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 185 ),  NOTE_SILENT( 145 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 200 ),  NOTE_SILENT(  25 ),     NOTE_C_6( 255 ),     NOTE_C_6( 200 ), 
         NOTE_SILENT(  25 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 185 ), 
         NOTE_SILENT( 145 ),     NOTE_G_5( 255 ),     NOTE_G_5( 200 ),  NOTE_SILENT(  25 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 200 ),  NOTE_SILENT(  25 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 146 ),  NOTE_SILENT(  49 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5(  38 ), 
         NOTE_SILENT(  97 ),     NOTE_G_5( 255 ),     NOTE_G_5( 200 ),  NOTE_SILENT(  25 ), 
            NOTE_A_5( 255 ),     NOTE_A_5( 200 ),  NOTE_SILENT(  25 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5(  86 ),  NOTE_SILENT(  19 ),     NOTE_G_5( 255 ),     NOTE_G_5(  86 ), 
         NOTE_SILENT(  19 ),    NOTE_AS_5( 227 ),  NOTE_SILENT(  13 ),     NOTE_D_6( 255 ), 
            NOTE_D_6(  86 ),  NOTE_SILENT(  19 ),    NOTE_AS_5( 255 ),    NOTE_AS_5(  86 ), 
         NOTE_SILENT(  19 ),     NOTE_D_6( 227 ),  NOTE_SILENT(  13 ),     NOTE_G_6( 255 ), 
            NOTE_G_6(  86 ),  NOTE_SILENT(  19 ),     NOTE_D_6( 255 ),     NOTE_D_6(  86 ), 
         NOTE_SILENT(  19 ),     NOTE_G_6( 227 ),  NOTE_SILENT(  13 ),     NOTE_A_6( 255 ), 
            NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 146 ),  NOTE_SILENT(  49 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 185 ),  NOTE_SILENT( 145 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 200 ),  NOTE_SILENT(  25 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 200 ),  NOTE_SILENT(  25 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 185 ),  NOTE_SILENT( 145 ),     NOTE_G_5( 255 ),     NOTE_G_5( 200 ), 
         NOTE_SILENT(  25 ),     NOTE_D_6( 255 ),     NOTE_D_6( 200 ),  NOTE_SILENT(  25 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 146 ), 
         NOTE_SILENT(  49 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5(  38 ),  NOTE_SILENT(  97 ),     NOTE_G_5( 255 ),     NOTE_G_5( 200 ), 
         NOTE_SILENT(  25 ),     NOTE_A_5( 255 ),     NOTE_A_5( 200 ),  NOTE_SILENT(  25 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5(  38 ), 
         NOTE_SILENT(  97 ),     NOTE_A_5(  77 ),     NOTE_G_5(  77 ),  NOTE_SILENT(   2 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 251 ),  NOTE_SILENT(  42 ), 
            NOTE_D_5( 255 ),     NOTE_D_5( 255 ),     NOTE_D_5( 255 ),     NOTE_D_5( 146 ), 
         NOTE_SILENT(  49 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 185 ), 
         NOTE_SILENT( 145 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 200 ),  NOTE_SILENT(  25 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 200 ),  NOTE_SILENT(  25 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 185 ),  NOTE_SILENT( 145 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 200 ),  NOTE_SILENT(  25 ),     NOTE_D_6( 255 ),     NOTE_D_6( 200 ), 
         NOTE_SILENT(  25 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 146 ),  NOTE_SILENT(  49 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5(  38 ),  NOTE_SILENT(  97 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 200 ),  NOTE_SILENT(  25 ),     NOTE_A_5( 255 ),     NOTE_A_5( 200 ), 
         NOTE_SILENT(  25 ),    NOTE_AS_5( 255 ),    NOTE_AS_5(  86 ),  NOTE_SILENT(  19 ), 
            NOTE_G_5( 255 ),     NOTE_G_5(  86 ),  NOTE_SILENT(  19 ),    NOTE_AS_5( 227 ), 
         NOTE_SILENT(  13 ),     NOTE_D_6( 255 ),     NOTE_D_6(  86 ),  NOTE_SILENT(  19 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5(  86 ),  NOTE_SILENT(  19 ),     NOTE_D_6( 227 ), 
         NOTE_SILENT(  13 ),     NOTE_G_6( 255 ),     NOTE_G_6(  86 ),  NOTE_SILENT(  19 ), 
            NOTE_D_6( 255 ),     NOTE_D_6(  86 ),  NOTE_SILENT(  19 ),     NOTE_G_6( 227 ), 
         NOTE_SILENT(  13 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ),     NOTE_A_6( 255 ), 
            NOTE_A_6( 146 ),  NOTE_SILENT(  49 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5(  38 ), 
  };
static const uint16_t Melody2_Length    = sizeof( Melody2 ) / sizeof(uint16_t);



// Track 3 - PokemonDPPT - VS Lake Guardians
static const uint16_t Melody3[] PROGMEM = { 
            NOTE_C_7( 255 ),     NOTE_C_7( 174 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 142 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 142 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 142 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 142 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 142 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 142 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ), 
            NOTE_C_6( 234 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_C_6( 200 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_G_6( 242 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_G_6( 202 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_C_7( 232 ), 
         NOTE_SILENT(  16 ),    NOTE_AS_6( 241 ),  NOTE_SILENT(  16 ),     NOTE_C_7( 197 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  54 ), 
            NOTE_C_6( 234 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_C_6( 200 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_G_6( 242 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_G_6( 202 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_C_7( 232 ), 
         NOTE_SILENT(  16 ),    NOTE_AS_6( 241 ),  NOTE_SILENT(  16 ),     NOTE_C_7( 197 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  54 ), 
            NOTE_C_6( 234 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_C_6( 200 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_G_6( 242 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_G_6( 202 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_G_6( 232 ), 
         NOTE_SILENT(  16 ),    NOTE_AS_6( 226 ),  NOTE_SILENT(  31 ),     NOTE_C_7( 197 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 221 ), 
         NOTE_SILENT(  35 ),     NOTE_C_6( 234 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ), 
            NOTE_A_6( 200 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_G_6( 242 ), 
         NOTE_SILENT(  16 ),     NOTE_F_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_G_6( 202 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ), 
            NOTE_G_6( 232 ),  NOTE_SILENT(  16 ),    NOTE_AS_6( 226 ),  NOTE_SILENT(  31 ), 
            NOTE_C_7( 197 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),    NOTE_GS_6( 255 ), 
           NOTE_GS_6( 221 ),  NOTE_SILENT(  35 ),     NOTE_C_6( 234 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT(  53 ),     NOTE_A_6( 200 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ), 
            NOTE_G_6( 242 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 231 ),  NOTE_SILENT(  16 ), 
            NOTE_G_6( 202 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT(  53 ),     NOTE_G_6( 232 ),  NOTE_SILENT(  16 ),    NOTE_AS_6( 226 ), 
         NOTE_SILENT(  31 ),     NOTE_C_7( 197 ),  NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 221 ),  NOTE_SILENT(  35 ),     NOTE_C_6( 234 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_A_6( 200 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT(  53 ),     NOTE_G_6( 242 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 231 ), 
         NOTE_SILENT(  16 ),     NOTE_G_6( 202 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT(  53 ),     NOTE_G_6( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_AS_6( 226 ),  NOTE_SILENT(  31 ),     NOTE_C_7( 197 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT(  53 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 221 ),  NOTE_SILENT(  35 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6(  29 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5(   1 ), 
         NOTE_SILENT(  31 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 216 ),  NOTE_SILENT(  31 ),     NOTE_C_5( 255 ),     NOTE_C_5( 224 ), 
         NOTE_SILENT(  31 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 218 ),  NOTE_SILENT(  31 ),     NOTE_C_5( 255 ),     NOTE_C_5( 219 ), 
         NOTE_SILENT(  31 ),     NOTE_G_4( 255 ),     NOTE_G_4( 251 ),  NOTE_SILENT(   5 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4(   1 ),  NOTE_SILENT(  31 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 216 ), 
         NOTE_SILENT(  31 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 223 ),  NOTE_SILENT(  31 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 227 ),  NOTE_SILENT(  16 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 251 ),  NOTE_SILENT(   5 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 253 ),  NOTE_SILENT(  31 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 219 ),  NOTE_SILENT(  31 ),     NOTE_G_4( 255 ),     NOTE_G_4( 224 ), 
         NOTE_SILENT(  31 ),     NOTE_C_5( 255 ),     NOTE_C_5( 224 ),  NOTE_SILENT(  31 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 239 ),  NOTE_SILENT(   5 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 255 ),     NOTE_G_4(   1 ),  NOTE_SILENT(  31 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 222 ),  NOTE_SILENT(  31 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 218 ),  NOTE_SILENT(  31 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 224 ),  NOTE_SILENT(  31 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 213 ),  NOTE_SILENT(  31 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 221 ),  NOTE_SILENT(  35 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  29 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 249 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 249 ),    NOTE_AS_6( 255 ), 
           NOTE_AS_6( 219 ),  NOTE_SILENT(  31 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 221 ), 
         NOTE_SILENT(  35 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6(  24 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 249 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ), 
           NOTE_CS_6( 255 ),    NOTE_CS_6( 246 ),  NOTE_SILENT(   5 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  29 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 219 ),  NOTE_SILENT(  31 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 224 ),  NOTE_SILENT(  31 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 255 ), 
           NOTE_AS_6( 255 ),    NOTE_AS_6( 249 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 246 ),  NOTE_SILENT(   5 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6(  24 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ),     NOTE_C_7( 249 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 252 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6(  29 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 249 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 249 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 219 ),  NOTE_SILENT(  31 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 221 ),  NOTE_SILENT(  35 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6(  24 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 249 ), 
           NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 246 ), 
         NOTE_SILENT(   5 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6(  29 ),     NOTE_E_6( 255 ),     NOTE_E_6( 219 ), 
         NOTE_SILENT(  31 ),     NOTE_F_6( 255 ),     NOTE_F_6( 224 ),  NOTE_SILENT(  31 ), 
           NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 249 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 246 ), 
         NOTE_SILENT(   5 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6(  24 ),     NOTE_B_5( 255 ),     NOTE_B_5( 224 ), 
         NOTE_SILENT(  31 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 241 ),  NOTE_SILENT(   5 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5(  24 ),     NOTE_D_5( 255 ), 
            NOTE_D_5( 255 ),     NOTE_D_5( 255 ),     NOTE_D_5( 224 ),  NOTE_SILENT(  31 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 219 ),  NOTE_SILENT(  31 ),     NOTE_D_5( 255 ), 
            NOTE_D_5( 255 ),     NOTE_D_5( 255 ),     NOTE_D_5( 246 ),  NOTE_SILENT(   5 ), 
            NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ), 
            NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ), 
            NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ), 
            NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ), 
            NOTE_E_5(  11 ),  NOTE_SILENT(   5 ),    NOTE_GS_5( 255 ),    NOTE_GS_5( 255 ), 
           NOTE_GS_5( 255 ),    NOTE_GS_5( 255 ),    NOTE_GS_5( 255 ),    NOTE_GS_5( 255 ), 
           NOTE_GS_5(  24 ),    NOTE_DS_5( 255 ),    NOTE_DS_5( 255 ),    NOTE_DS_5( 255 ), 
           NOTE_DS_5( 224 ),  NOTE_SILENT(  31 ),    NOTE_GS_5( 255 ),    NOTE_GS_5( 219 ), 
         NOTE_SILENT(  31 ),    NOTE_DS_5( 255 ),    NOTE_DS_5( 255 ),    NOTE_DS_5( 255 ), 
           NOTE_DS_5( 246 ),  NOTE_SILENT(   5 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5(  11 ),  NOTE_SILENT(   5 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6(  24 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 224 ),  NOTE_SILENT(  31 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 219 ),  NOTE_SILENT(  31 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 246 ),  NOTE_SILENT(   5 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ), 
            NOTE_E_6(  11 ),  NOTE_SILENT(   5 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ), 
           NOTE_GS_6(  24 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 224 ),  NOTE_SILENT(  31 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 219 ), 
         NOTE_SILENT(  31 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 246 ),  NOTE_SILENT(   5 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6(  11 ),  NOTE_SILENT(   5 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5(  29 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ), 
            NOTE_E_5( 249 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 249 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 219 ),  NOTE_SILENT(  31 ), 
           NOTE_GS_5( 255 ),    NOTE_GS_5( 221 ),  NOTE_SILENT(  35 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5(  24 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 249 ), 
           NOTE_CS_5( 255 ),    NOTE_CS_5( 255 ),    NOTE_CS_5( 255 ),    NOTE_CS_5( 246 ), 
         NOTE_SILENT(   5 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5(  29 ),     NOTE_E_5( 255 ),     NOTE_E_5( 219 ), 
         NOTE_SILENT(  31 ),     NOTE_F_5( 255 ),     NOTE_F_5( 224 ),  NOTE_SILENT(  31 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 249 ), 
           NOTE_GS_5( 255 ),    NOTE_GS_5( 255 ),    NOTE_GS_5( 255 ),    NOTE_GS_5( 246 ), 
         NOTE_SILENT(   5 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5(  11 ),  NOTE_SILENT(   5 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  29 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 249 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 249 ), 
           NOTE_AS_6( 255 ),    NOTE_AS_6( 219 ),  NOTE_SILENT(  31 ),    NOTE_GS_6( 255 ), 
           NOTE_GS_6( 221 ),  NOTE_SILENT(  35 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6(  24 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 249 ),    NOTE_CS_6( 255 ), 
           NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 246 ),  NOTE_SILENT(   5 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6(  29 ),     NOTE_E_6( 255 ),     NOTE_E_6( 219 ),  NOTE_SILENT(  31 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 224 ),  NOTE_SILENT(  31 ),    NOTE_CS_6( 255 ), 
           NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 249 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 246 ),  NOTE_SILENT(   5 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6(  11 ),  NOTE_SILENT(   5 ),     NOTE_E_7( 255 ),     NOTE_E_7( 255 ), 
            NOTE_E_7(   1 ),  NOTE_SILENT(  31 ),     NOTE_E_6( 233 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 243 ),  NOTE_SILENT(  16 ),     NOTE_E_6( 242 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_E_6( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 246 ),  NOTE_SILENT(  16 ),    NOTE_FS_6( 247 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_E_6( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 241 ),  NOTE_SILENT(  16 ),     NOTE_E_6( 241 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 232 ),  NOTE_SILENT(  16 ),     NOTE_E_6( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 243 ),  NOTE_SILENT(  20 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6(   1 ),  NOTE_SILENT(  31 ),     NOTE_C_5( 233 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 243 ),  NOTE_SILENT(  16 ),     NOTE_C_5( 242 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 231 ),  NOTE_SILENT(  16 ),     NOTE_C_5( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 246 ),  NOTE_SILENT(  16 ),     NOTE_D_5( 247 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 231 ),  NOTE_SILENT(  16 ),     NOTE_C_5( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 241 ),  NOTE_SILENT(  16 ),     NOTE_C_5( 241 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 232 ),  NOTE_SILENT(  16 ),     NOTE_C_5( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 243 ),  NOTE_SILENT(  20 ),     NOTE_E_7( 255 ),     NOTE_E_7( 255 ), 
            NOTE_E_7(   1 ),  NOTE_SILENT(  31 ),     NOTE_E_6( 233 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 243 ),  NOTE_SILENT(  16 ),     NOTE_E_6( 242 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_E_6( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 246 ),  NOTE_SILENT(  16 ),    NOTE_FS_6( 247 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_E_6( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 241 ),  NOTE_SILENT(  16 ),     NOTE_E_6( 241 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 232 ),  NOTE_SILENT(  16 ),     NOTE_E_6( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_F_6( 243 ),  NOTE_SILENT(  20 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6(   1 ),  NOTE_SILENT(  31 ),     NOTE_C_5( 233 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 243 ),  NOTE_SILENT(  16 ),     NOTE_C_5( 242 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 231 ),  NOTE_SILENT(  16 ),     NOTE_C_5( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 246 ),  NOTE_SILENT(  16 ),     NOTE_D_5( 247 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 231 ),  NOTE_SILENT(  16 ),     NOTE_C_5( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 241 ),  NOTE_SILENT(  16 ),     NOTE_C_5( 241 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 232 ),  NOTE_SILENT(  16 ),     NOTE_C_5( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_CS_5( 243 ),  NOTE_SILENT(  20 ),     NOTE_F_7( 255 ),     NOTE_F_7( 255 ), 
            NOTE_F_7(   1 ),  NOTE_SILENT(  31 ),     NOTE_F_6( 233 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 243 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 242 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 246 ),  NOTE_SILENT(  16 ),     NOTE_G_6( 247 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 241 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 241 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 232 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 243 ),  NOTE_SILENT(  20 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ), 
           NOTE_CS_6(   1 ),  NOTE_SILENT(  31 ),    NOTE_CS_5( 233 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 243 ),  NOTE_SILENT(  16 ),    NOTE_CS_5( 242 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 231 ),  NOTE_SILENT(  16 ),    NOTE_CS_5( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 246 ),  NOTE_SILENT(  16 ),    NOTE_DS_5( 247 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 231 ),  NOTE_SILENT(  16 ),    NOTE_CS_5( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 241 ),  NOTE_SILENT(  16 ),    NOTE_CS_5( 241 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 232 ),  NOTE_SILENT(  16 ),    NOTE_CS_5( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 243 ),  NOTE_SILENT(  20 ),     NOTE_F_7( 255 ),     NOTE_F_7( 255 ), 
            NOTE_F_7(   1 ),  NOTE_SILENT(  31 ),     NOTE_F_6( 233 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 243 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 242 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 246 ),  NOTE_SILENT(  16 ),     NOTE_G_6( 247 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 231 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 241 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 241 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 232 ),  NOTE_SILENT(  16 ),     NOTE_F_6( 232 ),  NOTE_SILENT(  16 ), 
           NOTE_FS_6( 243 ),  NOTE_SILENT(  20 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ), 
           NOTE_CS_6(   1 ),  NOTE_SILENT(  31 ),    NOTE_CS_5( 233 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 243 ),  NOTE_SILENT(  16 ),    NOTE_CS_5( 242 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 231 ),  NOTE_SILENT(  16 ),    NOTE_CS_5( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 246 ),  NOTE_SILENT(  16 ),    NOTE_DS_5( 247 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 231 ),  NOTE_SILENT(  16 ),    NOTE_CS_5( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 241 ),  NOTE_SILENT(  16 ),    NOTE_CS_5( 241 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 232 ),  NOTE_SILENT(  16 ),    NOTE_CS_5( 232 ),  NOTE_SILENT(  16 ), 
            NOTE_D_5( 243 ),  NOTE_SILENT(  20 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  29 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 249 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 249 ),    NOTE_AS_6( 255 ), 
           NOTE_AS_6( 219 ),  NOTE_SILENT(  31 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 221 ), 
         NOTE_SILENT(  35 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6(  24 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 249 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ), 
           NOTE_CS_6( 255 ),    NOTE_CS_6( 246 ),  NOTE_SILENT(   5 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  29 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 219 ),  NOTE_SILENT(  31 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 224 ),  NOTE_SILENT(  31 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 255 ), 
           NOTE_AS_6( 255 ),    NOTE_AS_6( 249 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 246 ),  NOTE_SILENT(   5 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6(  11 ), 
         NOTE_SILENT(   5 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6(  29 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 249 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 249 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 219 ), 
         NOTE_SILENT(  31 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 221 ),  NOTE_SILENT(  35 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6(  24 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 249 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ), 
           NOTE_CS_6( 246 ),  NOTE_SILENT(   5 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  29 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 219 ),  NOTE_SILENT(  31 ),     NOTE_F_6( 255 ),     NOTE_F_6( 224 ), 
         NOTE_SILENT(  31 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ), 
           NOTE_CS_6( 249 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 246 ),  NOTE_SILENT(   5 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  11 ),  NOTE_SILENT(   5 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6(  11 ),  NOTE_SILENT(   5 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  29 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  11 ), 
         NOTE_SILENT(   5 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6(  29 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  24 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5(  15 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 241 ), 
         NOTE_SILENT(   5 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5(  11 ),  NOTE_SILENT(   5 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5(  29 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6(  24 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5(  15 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 241 ),  NOTE_SILENT(   5 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5(  11 ), 
         NOTE_SILENT(   5 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6(  29 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5(  11 ),  NOTE_SILENT(   5 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5(  29 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5(  11 ),  NOTE_SILENT(   5 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5(  29 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 142 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 142 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ),    NOTE_AS_6( 255 ), 
           NOTE_AS_6( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ), 
           NOTE_AS_6( 255 ),    NOTE_AS_6( 145 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 142 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 145 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_G_6( 255 ),     NOTE_G_6( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_G_6( 255 ),     NOTE_G_6( 142 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ), 
           NOTE_AS_6( 255 ),    NOTE_AS_6( 145 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 142 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 142 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ),    NOTE_AS_6( 255 ), 
           NOTE_AS_6( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ), 
           NOTE_AS_6( 255 ),    NOTE_AS_6( 145 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 142 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 145 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_G_6( 255 ),     NOTE_G_6( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_G_6( 255 ),     NOTE_G_6( 142 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 109 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 180 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7( 145 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 105 ),     NOTE_C_7( 255 ),     NOTE_C_7(  46 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 205 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5(   1 ),  NOTE_SILENT(  31 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 216 ),  NOTE_SILENT(  31 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 224 ),  NOTE_SILENT(  31 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 218 ),  NOTE_SILENT(  31 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 219 ),  NOTE_SILENT(  31 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 251 ),  NOTE_SILENT(   5 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ), 
            NOTE_G_4(   1 ),  NOTE_SILENT(  31 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 216 ),  NOTE_SILENT(  31 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 223 ),  NOTE_SILENT(  31 ), 
            NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 255 ),     NOTE_C_5( 227 ), 
         NOTE_SILENT(  16 ),     NOTE_G_4( 255 ),     NOTE_G_4( 251 ),  NOTE_SILENT(   5 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 253 ), 
         NOTE_SILENT(  31 ),     NOTE_C_5( 255 ),     NOTE_C_5( 219 ),  NOTE_SILENT(  31 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 224 ),  NOTE_SILENT(  31 ),     NOTE_C_5( 255 ), 
            NOTE_C_5( 224 ),  NOTE_SILENT(  31 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 239 ), 
         NOTE_SILENT(   5 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4(   1 ), 
         NOTE_SILENT(  31 ),     NOTE_C_5( 255 ),     NOTE_C_5( 222 ),  NOTE_SILENT(  31 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 218 ), 
         NOTE_SILENT(  31 ),     NOTE_C_5( 255 ),     NOTE_C_5( 224 ),  NOTE_SILENT(  31 ), 
            NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 255 ),     NOTE_G_4( 213 ), 
         NOTE_SILENT(  31 ),     NOTE_C_5( 255 ),     NOTE_C_5( 221 ),  NOTE_SILENT(  35 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6(  29 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 249 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 249 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 219 ),  NOTE_SILENT(  31 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 221 ),  NOTE_SILENT(  35 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6(  24 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 249 ), 
           NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 246 ), 
         NOTE_SILENT(   5 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6(  29 ),     NOTE_E_6( 255 ),     NOTE_E_6( 219 ), 
         NOTE_SILENT(  31 ),     NOTE_F_6( 255 ),     NOTE_F_6( 224 ),  NOTE_SILENT(  31 ), 
           NOTE_AS_6( 255 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 255 ),    NOTE_AS_6( 249 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 246 ), 
         NOTE_SILENT(   5 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6(  24 ),     NOTE_C_7( 255 ),     NOTE_C_7( 255 ), 
            NOTE_C_7( 255 ),     NOTE_C_7( 249 ),  NOTE_SILENT( 255 ),  NOTE_SILENT( 255 ), 
         NOTE_SILENT( 255 ),  NOTE_SILENT( 252 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  29 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 249 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 255 ),     NOTE_F_6( 249 ),    NOTE_AS_6( 255 ), 
           NOTE_AS_6( 219 ),  NOTE_SILENT(  31 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 221 ), 
         NOTE_SILENT(  35 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6(  24 ),     NOTE_F_6( 255 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 255 ),     NOTE_F_6( 249 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ), 
           NOTE_CS_6( 255 ),    NOTE_CS_6( 246 ),  NOTE_SILENT(   5 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  29 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 219 ),  NOTE_SILENT(  31 ),     NOTE_F_6( 255 ), 
            NOTE_F_6( 224 ),  NOTE_SILENT(  31 ),    NOTE_CS_6( 255 ),    NOTE_CS_6( 255 ), 
           NOTE_CS_6( 255 ),    NOTE_CS_6( 249 ),    NOTE_AS_5( 255 ),    NOTE_AS_5( 255 ), 
           NOTE_AS_5( 255 ),    NOTE_AS_5( 246 ),  NOTE_SILENT(   5 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6(  24 ), 
            NOTE_B_5( 255 ),     NOTE_B_5( 224 ),  NOTE_SILENT(  31 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ),     NOTE_C_6( 255 ), 
            NOTE_C_6( 241 ),  NOTE_SILENT(   5 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ),     NOTE_G_5( 255 ), 
            NOTE_G_5(  24 ),     NOTE_D_5( 255 ),     NOTE_D_5( 255 ),     NOTE_D_5( 255 ), 
            NOTE_D_5( 224 ),  NOTE_SILENT(  31 ),     NOTE_G_5( 255 ),     NOTE_G_5( 219 ), 
         NOTE_SILENT(  31 ),     NOTE_D_5( 255 ),     NOTE_D_5( 255 ),     NOTE_D_5( 255 ), 
            NOTE_D_5( 246 ),  NOTE_SILENT(   5 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ), 
            NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ), 
            NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ), 
            NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5( 255 ), 
            NOTE_E_5( 255 ),     NOTE_E_5( 255 ),     NOTE_E_5(  11 ),  NOTE_SILENT(   5 ), 
           NOTE_GS_5( 255 ),    NOTE_GS_5( 255 ),    NOTE_GS_5( 255 ),    NOTE_GS_5( 255 ), 
           NOTE_GS_5( 255 ),    NOTE_GS_5( 255 ),    NOTE_GS_5(  24 ),    NOTE_DS_5( 255 ), 
           NOTE_DS_5( 255 ),    NOTE_DS_5( 255 ),    NOTE_DS_5( 224 ),  NOTE_SILENT(  31 ), 
           NOTE_GS_5( 255 ),    NOTE_GS_5( 219 ),  NOTE_SILENT(  31 ),    NOTE_DS_5( 255 ), 
           NOTE_DS_5( 255 ),    NOTE_DS_5( 255 ),    NOTE_DS_5( 246 ),  NOTE_SILENT(   5 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ),     NOTE_F_5( 255 ), 
            NOTE_F_5(  11 ),  NOTE_SILENT(   5 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ),     NOTE_G_6( 255 ), 
            NOTE_G_6(  24 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 224 ),  NOTE_SILENT(  31 ),     NOTE_G_6( 255 ),     NOTE_G_6( 219 ), 
         NOTE_SILENT(  31 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ),     NOTE_D_6( 255 ), 
            NOTE_D_6( 246 ),  NOTE_SILENT(   5 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6( 255 ), 
            NOTE_E_6( 255 ),     NOTE_E_6( 255 ),     NOTE_E_6(  11 ),  NOTE_SILENT(   5 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 255 ),    NOTE_GS_6(  24 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 224 ),  NOTE_SILENT(  31 ), 
           NOTE_GS_6( 255 ),    NOTE_GS_6( 219 ),  NOTE_SILENT(  31 ),    NOTE_DS_6( 255 ), 
           NOTE_DS_6( 255 ),    NOTE_DS_6( 255 ),    NOTE_DS_6( 246 ),  NOTE_SILENT(   5 ), 
  };
static const uint16_t Melody3_Length    = sizeof( Melody3 ) / sizeof(uint16_t);

 

////////////////////////// INIZIALIZZAZIONE DI COSTANTI E VARIABILI GLOBALI ////////////////////////////////////


#define INTERRUPT_SELECTION_PIN 2
#define INTERRUPT_PAUSE_PIN 3
#define SELECTION_PIN 4
#define TONE_PIN 8

 
#define NUMBER_OF_SONGS 4


#define TEMPO_SONG_0 1500
#define TEMPO_SONG_1 1000
#define TEMPO_SONG_2 1000
#define TEMPO_SONG_3 300
#define TEMPO_SONG_4 1000
#define TEMPO_SONG_5 1000
#define TEMPO_SONG_6 1000
#define TEMPO_SONG_7 1000
#define TEMPO_SONG_8 1000
#define TEMPO_SONG_9 1000
#define TEMPO_SONG_10 1000
#define TEMPO_SONG_11 1000
#define TEMPO_SONG_12 1000
#define TEMPO_SONG_13 1000
#define TEMPO_SONG_14 1000
#define TEMPO_SONG_15 1000


//VETTORE CON IL TEMPO DI CIASCUNA CANZONE SALVATA Espresso in microsecondi
unsigned long tempoToPlay[] = { TEMPO_SONG_0, TEMPO_SONG_1, TEMPO_SONG_2,
                TEMPO_SONG_3, TEMPO_SONG_4, TEMPO_SONG_5,
                TEMPO_SONG_6, TEMPO_SONG_7, TEMPO_SONG_8,
                TEMPO_SONG_9, TEMPO_SONG_10, TEMPO_SONG_11,
                TEMPO_SONG_12, TEMPO_SONG_13, TEMPO_SONG_14,
                TEMPO_SONG_15
                };

int lastEnabled = 0; // USATO NEGLI INTERRUPT PER EVITARE  CHE LONG PRESS CAUSINO MULTIPLI INTERRUPT
uint8_t songToPlay = 0; //AVENDO MOLTEPLICI CANZONI, SI PARTE DALLA PRIMA IN ARCHIVIO
boolean paused = true;
int selection;
boolean changeSong = false;


void setup() {
  Serial.begin(115200);
  pinMode(INTERRUPT_SELECTION_PIN,INPUT_PULLUP);
  pinMode(INTERRUPT_PAUSE_PIN,INPUT_PULLUP);
  pinMode(SELECTION_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_SELECTION_PIN), change, FALLING);
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_PAUSE_PIN), pause, FALLING);
}
void loop(){
    changeSong = false;
    melodyChoice(songToPlay);
}



//Funzione di Interrupt per la pausa
void pause() {
  if(millis() - lastEnabled > 300) {
    lastEnabled = millis();
    paused = !paused;
  }
}
//Il cambiamento di stato ha effetto all'interno della funzione playMelody



//Funzione di Interrupt per il cambio canzone
void change() {
  if(millis() - lastEnabled > 3000) {
	selection = digitalRead(SELECTION_PIN); // atrent: questo è il problema! (credo, spero) se la invochi DENTRO l'if almeno non ti cambia lo stato sempre e cmq
    lastEnabled = millis();
    changeSong = true;
/*
    //////////////////////////////////// DEBUGGING CODE
    Serial.print("CAMBIO CANZONE: ");
    if(selection)
      Serial.println("INDIETRO");
    else
      Serial.println("AVANTI");
    /////////////////////////////////// END DEBUGGING CODE
*/
    if(selection){
      if(songToPlay > 0)
        songToPlay--;
      else
        songToPlay = NUMBER_OF_SONGS - 1;
    }
    else
      songToPlay = (songToPlay + 1) % NUMBER_OF_SONGS;
  }
}


void playMelody(const uint16_t MelodyData[], const uint16_t MelodyLength, const unsigned long tempo){
  
  static const uint16_t Freq8[] PROGMEM = { 4186 , 4435 , 4699  , 4978 , 5274 , 5588 , 5920 , 6272 , 6645 , 7040 , 7459 , 7902 };

  for(uint16_t  x = 0; x < MelodyLength; x++){
    
   Serial.println(songToPlay);           //Segnala l'indice della melodia in riproduzione sulla Seriale
   if(changeSong){
    // Jingle per segnalare il cambio canzone
    tone(TONE_PIN, 2000);
    delay(250);
    noTone(TONE_PIN);
    tone(TONE_PIN, 2000);
    delay(250);
    noTone(TONE_PIN);
    delay(1500);
    return;
   }
   
   if(paused){
      digitalWrite(LED_BUILTIN, LOW);     //Segnala lo stato di Stop sulla Board
      noTone(TONE_PIN);                   //Segnala lo stato di Stop sulla Seriale
      x--;
      
      Serial.println("Not Playing");
      delay(300);
            continue;
    }

    digitalWrite(LED_BUILTIN, HIGH);      //Segnala lo stato di Sing sulla Board
    Serial.println("Playing");            //Segnala lo stato di Sing sulla Seriale


    //SE QUELLO CHE LEGGE È NOTA VUOTA, MANDA IL SEGNALE DI "NO TONE"
    uint16_t data = pgm_read_word((uint16_t *)&MelodyData[x]);
    if((data & 0xF) == 0xF) {
      noTone(TONE_PIN);
    }

    //IN CASO CONTRARIO CALCOLA LA FREQUENZA DELLA NOTA E LA MANDA AL BUZZER
    else{
      uint16_t Freq = pgm_read_word(&Freq8[data&0xF]) / ( 1 << (8-(data>>4 & 0xF)) );
      tone(TONE_PIN, Freq);  
    }    
    
    int16_t Duration = data>>8;
    while(Duration--) delayMicroseconds(tempo);
  }
}



inline static void melodyChoice(uint8_t melodyNumber)
{
  switch(melodyNumber)
  {
  #if NUMBER_OF_SONGS > 0
    case 0: { playMelody(Melody0, Melody0_Length, tempoToPlay[0]); return; }
  #endif
  #if NUMBER_OF_SONGS > 1
    case 1: { playMelody(Melody1, Melody1_Length, tempoToPlay[1]); return; }
  #endif
  #if NUMBER_OF_SONGS > 2
    case 2: { playMelody(Melody2, Melody2_Length, tempoToPlay[2]); return; }
  #endif
  #if NUMBER_OF_SONGS > 3
    case 3: { playMelody(Melody3, Melody3_Length, tempoToPlay[3]); return; }
  #endif
  #if NUMBER_OF_SONGS > 4
    case 4: { playMelody(Melody4, Melody4_Length, tempoToPlay[4]); return; }
  #endif
  #if NUMBER_OF_SONGS > 5
    case 5: { playMelody(Melody5, Melody5_Length, tempoToPlay[5]); return; }
  #endif
  #if NUMBER_OF_SONGS > 6
    case 6: { playMelody(Melody6, Melody6_Length, tempoToPlay[6]); return; }
  #endif
  #if NUMBER_OF_SONGS > 7
    case 7: { playMelody(Melody7, Melody7_Length, tempoToPlay[7]); return; }
  #endif
  #if NUMBER_OF_SONGS > 8
    case 8: { playMelody(Melody8, Melody8_Length, tempoToPlay[8]); return; }
  #endif
  #if NUMBER_OF_SONGS > 9
    case 9: { playMelody(Melody9, Melody9_Length, tempoToPlay[9]); return; }
  #endif
  #if NUMBER_OF_SONGS > 10
    case 10: { playMelody(Melody10, Melody10_Length, tempoToPlay[10]); return; }
  #endif
  #if NUMBER_OF_SONGS > 11
    case 11: { playMelody(Melody11, Melody11_Length, tempoToPlay[11]); return; }
  #endif
  #if NUMBER_OF_SONGS > 12
    case 12: { playMelody(Melody12, Melody12_Length, tempoToPlay[12]); return; }
  #endif
  #if NUMBER_OF_SONGS > 13
    case 13: { playMelody(Melody13, Melody13_Length, tempoToPlay[13]); return; }
  #endif
  #if NUMBER_OF_SONGS > 14
    case 14: { playMelody(Melody14, Melody14_Length, tempoToPlay[14]); return; }
  #endif
  #if NUMBER_OF_SONGS > 15
    case 15: { playMelody(Melody15, Melody15_Length, tempoToPlay[15]); return; }
  #endif
  }
}
